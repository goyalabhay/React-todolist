import { Button, Card, TextField } from "@mui/material";
import React, { useState } from "react";
import axios from "axios";
import { token } from "./ApiToken";
import { v4 as uuidv4 } from "uuid";

const AddTask = ({ setOpenTask, setAllTasks, allTasks, id }) => {
  const [taskName, setTaskName] = useState("");
  const [descriptionName, setDescriptionName] = useState("");

  async function createNewTask(id) {
    try {
      const url = "https://api.todoist.com/rest/v2/tasks";
      const requestData = {
        content: taskName,
        description: descriptionName,
        project_id: id,
      };
      const config = {
        headers: {
          "Content-Type": "application/json",
          "X-Request-Id": uuidv4(),
          Authorization: `Bearer ${token}`,
        },
      };

      const response = await axios.post(url, requestData, config);
      setAllTasks([...allTasks, response.data]);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  const handleTaskNameChange = (e) => {
    setTaskName(e.target.value);
  };

  const handleDescriptionChange = (e) => {
    setDescriptionName(e.target.value);
  };

  const handleSubmit = () => {
    createNewTask(id);
  };

  return (
    <div>
      <Card
        sx={{
          marginTop: "10px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          border: "1px solid lightgray",
          borderRadius: "10px",
          boxShadow: "none",
        }}
      >
        <TextField
          sx={{
            width: "100%",
            outline: "none",
          }}
          id="outlined-basic"
          placeholder="Task Name"
          value={taskName}
          onChange={handleTaskNameChange}
        />
        <TextField
          sx={{
            width: "100%",
            outline: "none",
          }}
          id="outlined-basic"
          placeholder="Description"
          value={descriptionName || ""}
          onChange={handleDescriptionChange}
        />

        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "flex-end",
            margin: "10px 10px 10px 0px",
          }}
        >
          <Button
            variant="text"
            sx={{
              marginRight: "10px",
              color: "grey",
              backgroundColor: "whitesmoke",
              "&:hover": {
                color: "black",
                backgroundColor: "#999",
              },
            }}
            onClick={() => setOpenTask(true)}
          >
            <b>Cancel</b>
          </Button>
          <Button
            className="addButton"
            variant="contained"
            sx={{
              marginLeft: "10px",
              backgroundColor: "rgb(200, 63, 63)",
              "&:hover": {
                backgroundColor: "rgb(97, 9, 9)",
              },
            }}
            onClick={handleSubmit}
          >
            <b>Add</b>
          </Button>
        </div>
      </Card>
    </div>
  );
};

export default AddTask;
