import { Button, Card, TextField } from "@mui/material";
import React, { useState } from "react";
import axios from "axios";
import { token } from "./ApiToken";
import { v4 as uuidv4 } from "uuid";

const EditTask = ({ task, setAllTasks, allTasks, setOpenEditTask }) => {
  const [editedTaskName, setEditedTaskName] = useState(task.content);
  const [editedDescriptionName, setEditedDescriptionName] = useState(
    task.description
  );

  async function updateTask() {
    try {
      const url = `https://api.todoist.com/rest/v2/tasks/${task.id}`;
      const requestData = {
        content: editedTaskName,
        description: editedDescriptionName,
      };
      const config = {
        headers: {
          "Content-Type": "application/json",
          "X-Request-Id": uuidv4(),
          Authorization: `Bearer ${token}`,
        },
      };

      const response = await axios.post(url, requestData, config);
      setAllTasks(
        allTasks.map((t) => {
          if (t.id === task.id) {
            task.content = response.data.content;
            task.description = response.data.description;
          }
          return t;
        })
      );
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  const handleEditSubmit = () => {
    updateTask();
    setOpenEditTask(true);
  };

  return (
    <div className="editTaskCard">
      <Card
        sx={{
          marginTop: "10px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          border: "1px solid lightgray",
          borderRadius: "10px",
          boxShadow: "none",
        }}
      >
        <TextField
          sx={{ width: "100%", outline: "none" }}
          id="outlined-basic"
          placeholder="Edit Task Name"
          value={editedTaskName}
          onChange={(e) => setEditedTaskName(e.target.value)}
          variant="outlined"
        />
        <TextField
          sx={{ width: "100%", outline: "none" }}
          id="outlined-basic"
          placeholder="Edit Description"
          variant="outlined"
          value={editedDescriptionName || ""}
          onChange={(e) => setEditedDescriptionName(e.target.value)}
        />
        <br />
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "flex-end",
            margin: "10px 10px 10px 0px",
          }}
        >
          <Button
            variant="text"
            sx={{
              marginRight: "10px",
              color: "grey",
              backgroundColor: "whitesmoke",
              "&:hover": {
                color: "black",
                backgroundColor: "#999",
              },
            }}
            onClick={() => setOpenEditTask(true)}
          >
            <b>Cancel</b>
          </Button>
          <Button
            variant="contained"
            sx={{
              marginLeft: "10px",
              backgroundColor: "rgb(200, 63, 63)",
              "&:hover": {
                backgroundColor: "rgb(97, 9, 9)",
              },
            }}
            onClick={handleEditSubmit}
          >
            <b>Edit</b>
          </Button>
        </div>
      </Card>
    </div>
  );
};

export default EditTask;
