import React, { useEffect, useState } from "react";
import axios from "axios";
import { token } from "./ApiToken";
import { useParams } from "react-router-dom";
import AddTask from "./AddTask";
import EachTask from "./EachTask";

const Tasks = ({ projects }) => {
  const [allTasks, setAllTasks] = useState([]);
  const [openTask, setOpenTask] = useState(true);

  const { id, name } = useParams();

  async function callAllTasks() {
    try {
      const response = await axios.get(
        "https://api.todoist.com/rest/v2/tasks",
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setAllTasks(response.data);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  useEffect(() => {
    callAllTasks();
  }, []);

  async function deleteTask(task) {
    try {
      const response = await axios.delete(
        `https://api.todoist.com/rest/v2/tasks/${task.id}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setAllTasks(allTasks.filter((t) => t.id !== task.id));
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  return (
    <div className="allTasks">
      <div>
        <div
          style={{
            fontSize: "20px",
            fontWeight: "bolder",
            padding: "10px",
            marginBottom: "20px",
          }}
        >
          {name}
        </div>
        {allTasks
          .filter((task) => task.project_id === id)
          .map((task) => (
            <EachTask
              key={task.id}
              deleteTask={deleteTask}
              task={task}
              setAllTasks={setAllTasks}
              allTasks={allTasks}
              name={name}
              projects={projects}
            />
          ))}
      </div>
      {openTask && (
        <div className="addtask" onClick={() => setOpenTask(false)}>
          <h3>+ Add task</h3>
        </div>
      )}
      {!openTask && (
        <div>
          <AddTask
            setOpenTask={setOpenTask}
            setAllTasks={setAllTasks}
            allTasks={allTasks}
            id={id}
            projects={projects}
          />
        </div>
      )}
    </div>
  );
};

export default Tasks;
