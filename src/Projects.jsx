import React, { useState } from "react";
import List from "@mui/material/List";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import axios from "axios";
import { token } from "./ApiToken";
import { Button } from "@mui/material";
import { v4 as uuidv4 } from "uuid";
import NewModal from "./NewModal";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import UpdateAndDeleteProject from "./UpdateAndDeleteProject";
import { Link } from "react-router-dom";

const Projects = ({ projects, setProjects }) => {
  const [newOpen, setNewOpen] = useState(false);
  const handleNewOpen = () => setNewOpen(true);
  const handleNewClose = () => setNewOpen(false);
  const [projectName, setProjectName] = useState("");

  const handleProjectNameChange = (e) => {
    setProjectName(e.target.value);
  };

  async function createNewProject() {
    try {
      const url = "https://api.todoist.com/rest/v2/projects";
      const requestData = {
        name: projectName,
      };
      const config = {
        headers: {
          "Content-Type": "application/json",
          "X-Request-Id": uuidv4(),
          Authorization: `Bearer ${token}`,
        },
      };

      const response = await axios.post(url, requestData, config);
      setProjects([...projects, response.data]);
      handleNewClose();
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  function handleSubmit() {
    createNewProject();
  }

  return (
    <>
      <Box
        sx={{
          display: "flex",
          backgroundColor: "whitesmoke",
          width: "fit-content",
          height: "94.2vh",
          overflowX: "hidden",
          boxShadow: "8px 0 6px -6px rgba(0, 0, 0, 0.5)",
        }}
      >
        <div className="flex">
          <Accordion sx={{ width: "300px", boxShadow: "none" }}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography>
                <h3>Favourites</h3>
              </Typography>
            </AccordionSummary>
            <Typography>
              <List sx={{ padding: "20px", width: "fit-content" }}>
                {projects
                  .filter((p) => p.is_favorite)
                  .map((project) => (
                    <Link
                      to={`/project/${project.id}/${project.name}`}
                      key={project.id}
                    >
                      <UpdateAndDeleteProject
                        project={project}
                        projects={projects}
                        setProjects={setProjects}
                      />
                    </Link>
                  ))}
              </List>
            </Typography>
          </Accordion>

          <Accordion sx={{ width: "300px", boxShadow: "none" }}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography>
                <h3>Projects</h3>
                <Button
                  onClick={handleNewOpen}
                  sx={{
                    width: "15px",
                    fontSize: "15px",
                    backgroundColor: "white",
                    borderRadius: "10px",
                    position: "absolute",
                    top: "10px",
                    left: "180px",
                    zIndex: "1",
                  }}
                >
                  +
                </Button>
              </Typography>
            </AccordionSummary>
            <Typography>
              <List sx={{ padding: "20px", width: "fit-content" }}>
                {projects.map((project) => (
                  <Link
                    to={`/project/${project.id}/${project.name}`}
                    key={project.id}
                  >
                    <UpdateAndDeleteProject
                      project={project}
                      projects={projects}
                      setProjects={setProjects}
                    />
                  </Link>
                ))}
              </List>
            </Typography>
          </Accordion>
        </div>
      </Box>
      <NewModal
        handleSubmit={handleSubmit}
        open={newOpen}
        handleClose={handleNewClose}
        handleProjectNameChange={handleProjectNameChange}
        projectName={projectName}
        setProjectName={setProjectName}
      />
    </>
  );
};

export default Projects;
