import React, { useState } from "react";
import { token } from "./ApiToken";
import { v4 as uuidv4 } from "uuid";
import axios from "axios";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import EditModal from "./EditModal";
import { ListItemText, Menu, MenuItem } from "@mui/material";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import DriveFileRenameOutlineIcon from "@mui/icons-material/DriveFileRenameOutline";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";

const UpdateAndDeleteProject = ({ project, projects, setProjects }) => {
  const [editedProjectName, setEditedProjectName] = useState(project.name);
  const [favorite, setFavorite] = useState(project.is_favorite);

  const [editOpen, setEditOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  const handleEditClose = () => setEditOpen(false);
  const handleEditOpen = () => setEditOpen(true);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleFavClick = async () => {
    const updatedFavorite = !favorite;
    setFavorite(updatedFavorite);
    await updateProject({ ...project, is_favorite: updatedFavorite });
    handleClose();
  };

  async function updateProject(updatedProject) {
    try {
      const url = `https://api.todoist.com/rest/v2/projects/${updatedProject.id}`;
      const requestData = {
        name: updatedProject.name,
        is_favorite: updatedProject.is_favorite,
      };
      const config = {
        headers: {
          "Content-Type": "application/json",
          "X-Request-Id": uuidv4(),
          Authorization: `Bearer ${token}`,
        },
      };
      await axios.post(url, requestData, config);
      setProjects((prevProjects) =>
        prevProjects.map((p) =>
          p.id === updatedProject.id ? updatedProject : p
        )
      );
      handleEditClose();
    } catch (error) {
      console.error("Error updating project:", error);
    }
  }

  async function deleteProject(project) {
    try {
      await axios.delete(
        `https://api.todoist.com/rest/v2/projects/${project.id}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setProjects((prevProjects) =>
        prevProjects.filter((p) => p.id !== project.id)
      );
    } catch (error) {
      console.error("Error deleting project:", error);
    }
  }

  function handleEditSubmit() {
    updateProject(project);
    handleClose();
  }

  return (
    <ListItem key={project.id}>
      <ListItemButton>
        <div className="eachProject">
          <EditModal
            handleEditSubmit={handleEditSubmit}
            open={editOpen}
            handleClose={handleEditClose}
            editedProjectName={editedProjectName}
            setEditedProjectName={setEditedProjectName}
          />
          <ListItemText
            sx={{ display: "flex", alignItems: "center" }}
            primary={project.name}
          />

          <div>
            <MoreHorizIcon onClick={handleClick} />
            <Menu
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <div className="projectButtons">
                <MenuItem
                  onClick={() => {
                    handleEditOpen();
                    handleClose();
                  }}
                >
                  <DriveFileRenameOutlineIcon />
                  Edit
                </MenuItem>
                <MenuItem
                  onClick={() => {
                    deleteProject(project);
                    handleClose();
                  }}
                >
                  <DeleteOutlineIcon />
                  Delete
                </MenuItem>
                <MenuItem onClick={handleFavClick}>
                  {favorite ? (
                    <>
                      <FavoriteBorderIcon />
                      Remove from Favorites
                    </>
                  ) : (
                    <>
                      <FavoriteBorderIcon />
                      Add to Favorites
                    </>
                  )}
                </MenuItem>
              </div>
            </Menu>
          </div>
        </div>
      </ListItemButton>
    </ListItem>
  );
};

export default UpdateAndDeleteProject;
