import React from "react";
import { Button, Modal, TextField } from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

const EditModal = ({
  handleEditSubmit,
  open,
  handleClose,
  editedProjectName,
  setEditedProjectName,
}) => {

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box
        sx={{
          borderRadius: "10px",
          width: "35%",
          backgroundColor: "white",
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
        }}
      >
        <div style={{ margin: "20px" }}>
          <Typography
            sx={{
              textAlign: "center",
              marginTop: "20px",
              fontSize: "20px",
              fontWeight: "bolder",
            }}
            id="modal-modal-title"
            variant="h6"
            component="h2"
          >
            <div>Edit Project</div>
          </Typography>
          <Typography sx={{ fontSize: "14px" }}>
            <b>Name</b>
          </Typography>
          <TextField
            sx={{ width: "100%", marginBottom: "20px" }}
            id="outlined-basic"
            variant="outlined"
            value={editedProjectName}
            onChange={(e) => setEditedProjectName(e.target.value)}
          />
          <div
            style={{
              marginBottom: "10px",
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Button
              variant="text"
              sx={{
                marginRight: "10px",
                color: "grey",
                backgroundColor: "whitesmoke",
                "&:hover": {
                  color: "black",
                  backgroundColor: "#999",
                },
              }}
              onClick={handleClose}
            >
              <b>Cancel</b>
            </Button>
            <Button
              variant="contained"
              sx={{
                marginLeft: "10px",
                backgroundColor: "rgb(200, 63, 63)",
                "&:hover": {
                  backgroundColor: "rgb(97, 9, 9)",
                },
              }}
              onClick={handleEditSubmit}
            >
              <b>Edit</b>
            </Button>
          </div>
        </div>
      </Box>
    </Modal>
  );
};

export default EditModal;
