import React from "react";

const Header = () => {
  return (
    <>
      <div className="header">
        <div className="headerText">Todoist</div>
      </div>
    </>
  );
};

export default Header;
