import React, { useState } from "react";
import EditTask from "./EditTask";
import { token } from "./ApiToken";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";
import Brightness1OutlinedIcon from "@mui/icons-material/Brightness1Outlined";
import CheckIcon from "@mui/icons-material/Check";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import DriveFileRenameOutlineIcon from "@mui/icons-material/DriveFileRenameOutline";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import axios from "axios";

const EachTask = ({
  projects,
  deleteTask,
  task,
  setAllTasks,
  allTasks,
  name,
}) => {
  const [openEditTask, setOpenEditTask] = useState(true);
  const [anchorEl, setAnchorEl] = useState(null);
  const [nestedAnchorEl, setNestedAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleNestedClick = (event) => {
    setNestedAnchorEl(event.currentTarget);
  };

  const handleNestedClose = () => {
    setNestedAnchorEl(null);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  async function moveTask(project, task) {
    try {
      const url = "https://api.todoist.com/rest/v2/tasks";
      const requestData = {
        content: task.content,
        description: task.description,
        project_id: project.id,
      };
      const config = {
        headers: {
          "Content-Type": "application/json",
          "X-Request-Id": task.id,
          Authorization: `Bearer ${token}`,
        },
      };

      const newResponse = await axios.post(url, requestData, config);

      await axios.delete(`https://api.todoist.com/rest/v2/tasks/${task.id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      setAllTasks([
        ...allTasks.filter((t) => t.id !== task.id),
        newResponse.data,
      ]);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  return (
    <div className="editTask"
    >
      {openEditTask && (
        <>
          <div style={{ display: "flex", columnGap: "10px" }}>
            <div>
              <Brightness1OutlinedIcon
                sx={{ cursor: "pointer" }}
                onClick={() => deleteTask(task)}
              />
            </div>

            <div>
              <div style={{ fontSize: "16px", marginBottom: "10px" }}>
                {task.content}
              </div>
              <div style={{ fontSize: "12px", color: "grey" }}>
                {task.description}
              </div>
            </div>
          </div>

          <div style={{ display: "flex" }}>
            <Button
              aria-controls="simple-menu"
              aria-haspopup="true"
              onClick={handleClick}
              sx={{ boxShadow: "none" }}
            >
              <MoreHorizIcon />
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem
                onClick={() => {
                  handleClose();
                  setOpenEditTask(false);
                }}
              >
                <DriveFileRenameOutlineIcon />
                <span>Edit</span>
              </MenuItem>
              <MenuItem
                onClick={() => {
                  handleClose();
                  deleteTask(task);
                }}
              >
                <DeleteOutlineIcon />
                <span>Delete</span>
              </MenuItem>
              <MenuItem onClick={handleNestedClick}>
                Move to Project <KeyboardArrowDownIcon />
              </MenuItem>
            </Menu>

            <Menu
              id="nested-menu"
              anchorEl={nestedAnchorEl}
              open={Boolean(nestedAnchorEl)}
              onClose={handleNestedClose}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
            >
              {projects.map((eachProject) => (
                <MenuItem
                  key={eachProject.id}
                  onClick={() => {
                    moveTask(eachProject, task);
                    handleNestedClose();
                    handleClose();
                  }}
                >
                  {name === eachProject.name ? (
                    <>
                      <span>{eachProject.name}</span>
                      <CheckIcon />
                    </>
                  ) : (
                    <span>{eachProject.name}</span>
                  )}
                </MenuItem>
              ))}
            </Menu>
          </div>
        </>
      )}
      {!openEditTask && (
        <EditTask
          task={task}
          setAllTasks={setAllTasks}
          allTasks={allTasks}
          setOpenEditTask={setOpenEditTask}
        />
      )}
    </div>
  );
};

export default EachTask;
