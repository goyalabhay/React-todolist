import React from "react";
import { Button, Modal, TextField } from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

const NewModal = ({
  handleSubmit,
  open,
  handleClose,
  handleProjectNameChange,
  projectName,
  setProjectName,
}) => {
  const handleFormSubmit = () => {
    handleSubmit(projectName);
    setProjectName("");
  };

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box
        sx={{
          borderRadius: "10px",
          width: "35%",
          backgroundColor: "white",
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
        }}
      >
        <div style={{ margin: "20px" }}>
          <Typography
            sx={{
              marginTop: "20px",
              fontSize: "20px",
              fontWeight: "bolder",
              marginBottom: "20px",
            }}
            id="modal-modal-title"
            variant="h6"
            component="h2"
          >
            <div>Add Project</div>
          </Typography>
          <Typography sx={{ fontSize: "14px" }}>
            <b>Name</b>
          </Typography>
          <TextField
            sx={{ width: "100%", marginBottom: "20px" }}
            id="outlined-basic"
            value={projectName}
            onChange={handleProjectNameChange}
            variant="outlined"
          />
          <div
            style={{
              marginBottom: "10px",
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Button
              variant="text"
              sx={{
                marginRight: "10px",
                color: "grey",
                backgroundColor: "whitesmoke",
                "&:hover": {
                  color: "black",
                  backgroundColor: "#999",
                },
              }}
              onClick={handleClose}
            >
              <b>Cancel</b>
            </Button>
            <Button
              variant="contained"
              sx={{
                marginLeft: "10px",
                backgroundColor: "rgb(200, 63, 63)",
                "&:hover": {
                  backgroundColor: "rgb(97, 9, 9)",
                },
              }}
              onClick={handleFormSubmit}
            >
              <b>Add</b>
            </Button>
          </div>
        </div>
      </Box>
    </Modal>
  );
};

export default NewModal;
