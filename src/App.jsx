import React, { useEffect, useState } from "react";
import Header from "./Header";
import Projects from "./Projects";
import Tasks from "./Tasks";
import { Route, Routes } from "react-router-dom";
import axios from "axios";
import { token } from "./ApiToken";

const App = () => {
  const [projects, setProjects] = useState([]);

  async function callAllProjects() {
    try {
      const response = await axios.get(
        "https://api.todoist.com/rest/v2/projects",
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      response.data.shift();
      setProjects(response.data);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  useEffect(() => {
    callAllProjects();
  }, []);

  return (
    <div>
      <Header />
      <div className="containerFlex">
        <Projects projects={projects} setProjects={setProjects} />
        <Routes>
          <Route
            path="/project/:id/:name"
            element={<Tasks projects={projects} />}
          ></Route>
        </Routes>
      </div>
    </div>
  );
};

export default App;
